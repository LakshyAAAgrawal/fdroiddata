Categories:Theming
License:WTFPL
Web Site:https://github.com/dotWee/MicroPinner/blob/HEAD/README.md
Source Code:https://github.com/dotWee/MicroPinner
Issue Tracker:https://github.com/dotWee/MicroPinner/issues

Auto Name:MicroPinner
Summary:Test and customize the statusbar
Description:
Lightweight dialog-only application, which lets you pin text to your statusbar.
You can also customize the pins priority and visibility (Android 5.+ only).

Might send crash reports.
.

Repo Type:git
Repo:https://github.com/dotWee/MicroPinner

Build:v1.5.1,16
    commit=release-v1.5.1
    subdir=app
    gradle=yes

Build:v1.5.2,17
    commit=release-v1.5.2
    subdir=app
    gradle=yes

Build:v1.6,18
    commit=release-v1.6
    subdir=app
    gradle=yes

Build:v1.8,21
    commit=release-v1.8
    subdir=app
    gradle=prod

Auto Update Mode:Version release-%v
Update Check Mode:Tags
Current Version:v1.8
Current Version Code:21
