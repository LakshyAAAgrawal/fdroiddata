Categories:Money
License:GPLv2
Web Site:http://www.codelathe.com/mmex
Source Code:https://github.com/moneymanagerex/android-money-manager-ex
Issue Tracker:https://github.com/moneymanagerex/android-money-manager-ex/issues

Auto Name:Money Manager Ex
Summary:Money management and expenses tracking
Description:
A simple and easy-to-use application to manage personal finances, bank accounts,
family budget, and so on. It's main goal is to add a mobile implementation for
an application we love to use on our Desktop machines.

Features:

* Management of single transactions
* Account, payee, category, currency management
* Storage card Import/Export
* Dropbox synchronization
* Security, application lock/unlock
* Repeating transactions
* Charts
* Supported languages: English, Italian, Portoguese, Spanish and Russian
.

Repo Type:git
Repo:https://github.com/moneymanagerex/android-money-manager-ex

Build:2.15.2,687
    disable=third party maven repo, jar
    commit=2.15.2
    gradle=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:2015.12.09
Current Version Code:752
