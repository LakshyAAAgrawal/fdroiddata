AntiFeatures:NonFreeNet
Categories:Multimedia,Internet
License:GPLv3+
Web Site:http://dasochan.nl/newpipe
Source Code:https://github.com/theScrabi/NewPipe
Issue Tracker:https://github.com/theScrabi/NewPipe/issues

Auto Name:NewPipe
Summary:Lightweight YouTube frontend
Description:
Lightweight YouTube frontend that's supposed to be used without the proprietary
YouTube-API or any of Google's (proprietary) play-services. NewPipe only parses
the YouTube website in order to gain the information it needs.

This a very early version of the app, so not all functionality is implemented,
and there may still be a lot of bugs. But all in all it's doing what it is
supposed to do. It makes it possible to watch YouTube videos. So don't be cruel
to this app. It will improve...
.

Repo Type:git
Repo:https://github.com/theScrabi/NewPipe

Build:0.3,1
    commit=v0.3
    subdir=app
    gradle=yes

Build:0.3.5,2
    commit=d3cc5185290ff41e901cb0d55f0e4a0a52712159
    subdir=app
    gradle=yes

Build:0.4.1,4
    commit=v0.4.1
    subdir=app
    gradle=yes

Build:0.5.0,5
    commit=v0.5.0
    subdir=app
    gradle=yes

Build:0.6.0,6
    commit=v0.6.0
    subdir=app
    gradle=yes

Build:0.6.1,7
    commit=v0.6.1
    subdir=app
    gradle=yes

Build:0.6.2,8
    commit=v0.6.2
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:0.6.2
Current Version Code:8
