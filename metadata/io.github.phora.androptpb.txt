Categories:Internet
License:GPLv3
Web Site:
Source Code:https://github.com/phora/AndroPTPB
Issue Tracker:https://github.com/phora/AndroPTPB/issues

Auto Name:AndroPTPB
Summary:Post snippets and files with the ptpb service
Description:
Client for the pastebin service located at ptpb.pw. Lets you quickly upload code
snippets and small binary files. Also can replace the contents of pastes you've
uploaded earlier if you need to and can edit mimetype/highlighting hints for
your pastes.
.

Repo Type:git
Repo:https://github.com/phora/AndroPTPB

Build:1.0,1
    commit=1.0
    subdir=app
    gradle=yes

Auto Update Mode:Version %v
Update Check Mode:Tags ^[0-9\.]+$
# Beta releases will be tagged as v%v-beta and should not be build by F-Droid
Current Version:1.0
Current Version Code:1
