Categories:Games
License:MIT
Web Site:http://w84death.itch.io/tanks-of-freedom
Source Code:https://github.com/w84death/Tanks-of-Freedom
Issue Tracker:https://github.com/w84death/Tanks-of-Freedom/issues
Bitcoin:18oHovhxpevALZFcjH3mgNKB1yLi3nNFRY

Summary:Indie Turn Based Strategy in Isometric Pixel Art
Description:
Classic turn-based strategy game with two armies fighting against each other.
Challenge yourself in campaign against AI or hot-seat battle with a friend.
Using easy map editor you can create any imaginable scenario to play and share
online for others to discover!

Each 32x32 sprite was handcrafted in gorgeous, 16 colour pixel art. Original
soundtrack was made on Access Virus C and Korg Electribe. Sound effects were
made on real GameBoy hardware. Game is open-source and runs smoothly thanks to
Godot Engine.
.

Repo Type:git
Repo:https://github.com/w84death/Tanks-of-Freedom.git

Build:0.1,1
    commit=425f5328dba8414afabf72fcab6d572e0dbbebb4
    output=tof.apk
    srclibs=Godot@d3b42e41b0e446938d6634c8783e575de3d0b60f
    prebuild=mkdir tof && \
        bash -O extglob -c "mv !(tof) tof" && \
        printf "[platform:Android]\n\npackage/icon=\"res://icon.png\"\npackage/name=\"Tanks of Freedom\"\npackage/unique_name=\"in.p1x.tanks_of_freedom\"\nversion/code=\"1\"\nversion/name=\"0.1\"\n" > tof/export.cfg && \
        cp -R $$Godot$$ godot
    target=android-19
    scandelete=godot/platform/flash/lib/libGL.a,godot/platform/android/libs/google_play_services/libs/google-play-services.jar
    build=cd godot && \
        scons -j 8 p=server && \
        for libname in apk_expansion downloader_library google_play_services play_licensing; do sed -i 's/android-15/android-19/' platform/android/libs/${libname}/project.properties; done && \
        export ANDROID_NDK_ROOT=$$NDK$$ && \
        scons -j 8 p=android target=release && \
        mkdir -p platform/android/java/libs/armeabi && \
        cp bin/libgodot.android.opt.so platform/android/java/libs/armeabi/libgodot_android.so && \
        pushd platform/android/java/ && \
        ant release && \
        popd && \
        export HOME=$PWD && \
        mkdir -p ~/.godot/templates/ && \
        cp platform/android/java/bin/Godot-release-unsigned.apk ~/.godot/templates/android_release.apk && \
        cd ../tof && \
        ../godot/bin/godot_server.server.tools.* -export Android ../tof.apk

Build:0.3.3-beta,2
    commit=c6e779864e8c4d4b459850819b74d0e89202f2b8
    output=tof.apk
    srclibs=Godot@d3b42e41b0e446938d6634c8783e575de3d0b60f
    prebuild=mkdir tof && \
        bash -O extglob -c "mv !(tof) tof" && \
        printf "[platform:Android]\n\npackage/icon=\"res://icon.png\"\npackage/name=\"Tanks of Freedom\"\npackage/unique_name=\"in.p1x.tanks_of_freedom\"\nversion/code=\"2\"\nversion/name=\"0.3.3-beta\"\n" > tof/export.cfg && \
        cp -R $$Godot$$ godot
    target=android-19
    scandelete=godot/platform/flash/lib/libGL.a,godot/platform/android/libs/google_play_services/libs/google-play-services.jar
    build=cd godot && \
        scons -j 8 p=server && \
        for libname in apk_expansion downloader_library google_play_services play_licensing; do sed -i 's/android-15/android-19/' platform/android/libs/${libname}/project.properties; done && \
        export ANDROID_NDK_ROOT=$$NDK$$ && \
        scons -j 8 p=android target=release && \
        mkdir -p platform/android/java/libs/armeabi && \
        cp bin/libgodot.android.opt.so platform/android/java/libs/armeabi/libgodot_android.so && \
        pushd platform/android/java/ && \
        ant release && \
        popd && \
        export HOME=$PWD && \
        mkdir -p ~/.godot/templates/ && \
        cp platform/android/java/bin/Godot-release-unsigned.apk ~/.godot/templates/android_release.apk && \
        cd ../tof && \
        ../godot/bin/godot_server.server.tools.* -export Android ../tof.apk

Build:0.3.4-beta,3
    commit=29c2902a66e463e88d26f92bb3ab1d97f1e7e564
    output=tof.apk
    srclibs=Godot@d3b42e41b0e446938d6634c8783e575de3d0b60f
    prebuild=mkdir tof && \
        bash -O extglob -c "mv !(tof) tof" && \
        printf "[platform:Android]\n\npackage/icon=\"res://icon.png\"\npackage/name=\"Tanks of Freedom\"\npackage/unique_name=\"in.p1x.tanks_of_freedom\"\nversion/code=\"3\"\nversion/name=\"0.3.4-beta\"\n" > tof/export.cfg && \
        cp -R $$Godot$$ godot
    target=android-19
    scandelete=godot/platform/flash/lib/libGL.a,godot/platform/android/libs/google_play_services/libs/google-play-services.jar
    build=cd godot && \
        scons -j 8 p=server && \
        for libname in apk_expansion downloader_library google_play_services play_licensing; do sed -i 's/android-15/android-19/' platform/android/libs/${libname}/project.properties; done && \
        export ANDROID_NDK_ROOT=$$NDK$$ && \
        scons -j 8 p=android target=release && \
        mkdir -p platform/android/java/libs/armeabi && \
        cp bin/libgodot.android.opt.so platform/android/java/libs/armeabi/libgodot_android.so && \
        pushd platform/android/java/ && \
        ant release && \
        popd && \
        export HOME=$PWD && \
        mkdir -p ~/.godot/templates/ && \
        cp platform/android/java/bin/Godot-release-unsigned.apk ~/.godot/templates/android_release.apk && \
        cd ../tof && \
        ../godot/bin/godot_server.server.tools.* -export Android ../tof.apk

Build:0.3.5-beta,4
    commit=71af1792c2cdfea088f3dd77012c814d755ff790
    output=tof.apk
    srclibs=Godot@d3b42e41b0e446938d6634c8783e575de3d0b60f
    prebuild=mkdir tof && \
        bash -O extglob -c "mv !(tof) tof" && \
        printf "[platform:Android]\n\npackage/icon=\"res://icon.png\"\npackage/name=\"Tanks of Freedom\"\npackage/unique_name=\"in.p1x.tanks_of_freedom\"\nversion/code=\"4\"\nversion/name=\"0.3.5-beta\"\n" > tof/export.cfg && \
        cp -R $$Godot$$ godot
    target=android-19
    scandelete=godot/platform/flash/lib/libGL.a,godot/platform/android/libs/google_play_services/libs/google-play-services.jar
    build=cd godot && \
        scons -j 8 p=server && \
        for libname in apk_expansion downloader_library google_play_services play_licensing; do sed -i 's/android-15/android-19/' platform/android/libs/${libname}/project.properties; done && \
        export ANDROID_NDK_ROOT=$$NDK$$ && \
        scons -j 8 p=android target=release && \
        mkdir -p platform/android/java/libs/armeabi && \
        cp bin/libgodot.android.opt.so platform/android/java/libs/armeabi/libgodot_android.so && \
        pushd platform/android/java/ && \
        ant release && \
        popd && \
        export HOME=$PWD && \
        mkdir -p ~/.godot/templates/ && \
        cp platform/android/java/bin/Godot-release-unsigned.apk ~/.godot/templates/android_release.apk && \
        cd ../tof && \
        ../godot/bin/godot_server.server.tools.* -export Android ../tof.apk

Build:0.3.6.1-beta,6
    commit=bb5c6698dc3602db9ba28209d240a679573a95d1
    output=tof.apk
    srclibs=Godot@d3b42e41b0e446938d6634c8783e575de3d0b60f
    prebuild=mkdir tof && \
        bash -O extglob -c "mv !(tof) tof" && \
        printf "[platform:Android]\n\npackage/icon=\"res://icon.png\"\npackage/name=\"Tanks of Freedom\"\npackage/unique_name=\"in.p1x.tanks_of_freedom\"\nversion/code=\"6\"\nversion/name=\"0.3.6.1-beta\"\n" > tof/export.cfg && \
        cp -R $$Godot$$ godot
    target=android-19
    scandelete=godot/platform/flash/lib/libGL.a,godot/platform/android/libs/google_play_services/libs/google-play-services.jar
    build=cd godot && \
        scons -j 8 p=server && \
        for libname in apk_expansion downloader_library google_play_services play_licensing; do sed -i 's/android-15/android-19/' platform/android/libs/${libname}/project.properties; done && \
        export ANDROID_NDK_ROOT=$$NDK$$ && \
        scons -j 8 p=android target=release && \
        mkdir -p platform/android/java/libs/armeabi && \
        cp bin/libgodot.android.opt.so platform/android/java/libs/armeabi/libgodot_android.so && \
        pushd platform/android/java/ && \
        ant release && \
        popd && \
        export HOME=$PWD && \
        mkdir -p ~/.godot/templates/ && \
        cp platform/android/java/bin/Godot-release-unsigned.apk ~/.godot/templates/android_release.apk && \
        cd ../tof && \
        ../godot/bin/godot_server.server.tools.* -export Android ../tof.apk

Build:0.3.7-beta,7
    commit=f70b833660371786493b37de14f3ace4c24cd1b5
    output=tof.apk
    srclibs=Godot@d3b42e41b0e446938d6634c8783e575de3d0b60f
    prebuild=mkdir tof && \
        bash -O extglob -c "mv !(tof) tof" && \
        printf "[platform:Android]\n\npackage/icon=\"res://icon.png\"\npackage/name=\"Tanks of Freedom\"\npackage/unique_name=\"in.p1x.tanks_of_freedom\"\nversion/code=\"7\"\nversion/name=\"0.3.7-beta\"\n" > tof/export.cfg && \
        cp -R $$Godot$$ godot
    target=android-19
    scandelete=godot/platform/flash/lib/libGL.a,godot/platform/android/libs/google_play_services/libs/google-play-services.jar,godot/drivers/builtin_openssl2/crypto/pkcs7/p7/
    build=cd godot && \
        scons -j 8 p=server && \
        for libname in apk_expansion downloader_library google_play_services play_licensing; do sed -i 's/android-15/android-19/' platform/android/libs/${libname}/project.properties; done && \
        export ANDROID_NDK_ROOT=$$NDK$$ && \
        scons -j 8 p=android target=release && \
        mkdir -p platform/android/java/libs/armeabi && \
        cp bin/libgodot.android.opt.so platform/android/java/libs/armeabi/libgodot_android.so && \
        pushd platform/android/java/ && \
        ant release && \
        popd && \
        export HOME=$PWD && \
        mkdir -p ~/.godot/templates/ && \
        cp platform/android/java/bin/Godot-release-unsigned.apk ~/.godot/templates/android_release.apk && \
        cd ../tof && \
        ../godot/bin/godot_server.server.tools.* -export Android ../tof.apk

Auto Update Mode:None
Update Check Mode:None
